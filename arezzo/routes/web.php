<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function () {//permite acessar  o conteúdo após logado

Route::get('/',['as'=>'admin.index', 'uses'=>'ContatoController@index']);

Route::get('/create',  ['as'=>'admin.create', 'uses'=>'ContatoController@create']);
Route::get('/admin.edit{id}', 'ContatoController@edit')->name('edit');
Route::get('/destroy{id}', 'ContatoController@destroy')->name('destroy');
Route::post('/update{id}', 'ContatoController@update')->name('update');
Route::post('/store', 'ContatoController@store')->name('store');
});

Auth::routes();
